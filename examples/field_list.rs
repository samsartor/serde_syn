extern crate field_list_macros;

use field_list_macros::FieldList;

trait FieldList {
    fn fields() -> &'static[(&'static str, &'static str)];
}

#[derive(FieldList)]
pub struct Foo {
    pub foo: String,
    #[field(rename="world")]
    pub hello: u8,
    #[field(skip)]
    pub phantom: (),
}

fn main() {
    println!("{:?}", Foo::fields());
}
