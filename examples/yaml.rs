extern crate yaml_macros;

use yaml_macros::yaml;

yaml! { Input(
    name("ATTRY"),
    count(5),
    text("ZO RELAXEN UND WATSCHEN DER BLINKENLICHTEN."),
    hashmap(
        [] => 0,
    ),
    oneof(MyVarient(
        data("Hello", "World"),
    )),
) }

yaml! {
    name: "EXPRY",
    text: "Coffee is a way of stealing time that should by rights belong \
        to your older self.",
    bytes: b"\x00\x01\x02\x03",
    hashmap: {
        [1, 2, 3] => 6,
        [1, 2, 3, 5] => 11,
    },
    oneof: MyVarient {
        data: ["Foo", "Bar"],
    },
}

fn main() {
    println!("{}", ATTRY);
    println!("{}", EXPRY);
}
