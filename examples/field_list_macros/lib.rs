extern crate proc_macro;

use serde_derive::{Deserialize};
use serde_syn::{parser, config};
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Field};
use syn::visit::{Visit, visit_derive_input};

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct AttrProps {
    rename: Option<String>,
    skip: Option<()>,
}

#[derive(Default)]
struct FieldVisitor {
    tuples: Vec<TokenStream2>,
}

impl<'ast> Visit<'ast> for FieldVisitor {
    fn visit_field(&mut self, node: &'ast Field) {
        let mut name = match node.ident {
            Some(ref ident) => ident.to_string(),
            None => return,
        };

        for attr in node.attrs.iter().filter(|a| a.path.is_ident("field")) {
            let parser = parser::<AttrProps>(config::RUSTY_META);
            let props = match attr.parse_args_with(parser) {
                Ok(props) => props,
                Err(err) => {
                    self.tuples.push(err.to_compile_error());
                    return;
                },
            };

            if let Some(rename) = props.rename {
                name = rename;
            }
            if props.skip.is_some() {
                return;
            }
        }

        let ty = &node.ty;
        self.tuples.push(quote! { (#name, stringify!(#ty)) });
    }
}

#[proc_macro_derive(FieldList, attributes(field))]
pub fn my_macro(input: TokenStream) -> TokenStream {
    let mut visitor = FieldVisitor::default();
    let input = parse_macro_input!(input as DeriveInput);
    visit_derive_input(&mut visitor, &input);

    let tuples = visitor.tuples;
    let ident = input.ident;
    (quote! {
        impl FieldList for #ident {
            fn fields() -> &'static[(&'static str, &'static str)] {
                &[#(#tuples,)*]
            }
        }
    }).into()
}
