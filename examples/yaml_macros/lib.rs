extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::Span;
use syn::{parse_macro_input, Ident};
use syn::parse::{Parse, ParseStream};
use serde_yaml::to_string;
use serde_bytes::ByteBuf;
use serde_derive::{Deserialize, Serialize};
use serde_syn::{from_stream, config};
use quote::quote;
use std::collections::HashMap;

#[derive(Deserialize, Serialize)]
enum MyEnum {
    MyVarient { data: Vec<String> }
}

#[derive(Deserialize, Serialize)]
struct Input {
    name: String,
    text: Option<String>,
    bytes: Option<ByteBuf>,
    #[serde(default)]
    count: usize,
    hashmap: HashMap<Vec<u32>, u32>,
    oneof: MyEnum,
}


impl Parse for Input {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        from_stream(config::ANYTHING_GOES, input)
    }
}

#[proc_macro]
pub fn yaml(input: TokenStream) -> TokenStream {
    let i = parse_macro_input!(input as Input);
    let s = to_string(&i).unwrap();
    let n = Ident::new(&i.name, Span::call_site());
    (quote! { const #n: &str = #s; }).into()
}
