#[macro_use]
mod utils;

#[test]
fn bool() {
    assert_parse!(bool, true);
    assert_parse!(bool, false);
}

#[test]
fn char() {
    assert_parse!(char, '∰');
}

#[test]
fn uint() {
    assert_parse!(u32, 42);
    assert_parse!(u64, 42);
}

#[test]
fn suffixed_uint() {
    assert_parse!(u32, 42u32);
    assert_parse!(u64, 42u64);
}

#[test]
fn int() {
    assert_parse!(i32, -42);
    assert_parse!(i64, -42);
}

#[test]
fn suffixed_int() {
    assert_parse!(i32, -42i32);
    assert_parse!(i64, -42i64);
}

#[test]
fn float() {
    assert_parse!(f32, 3.14);
    assert_parse!(f64, 3.14);
}

#[test]
fn suffixed_float() {
    assert_parse!(f32, 3.14f32);
    assert_parse!(f64, 3.14f64);
}

#[test]
fn neg_float() {
    assert_parse!(f32, -3.14f32);
    assert_parse!(f64, -3.14f64);
}

#[test]
fn overflow_error() {
    assert_nparse!(u8 => "number too large to fit in target type", 256);
}

#[test]
fn neg_overflow_error() {
    assert_nparse!(i8 => "number too large to fit in target type", -129);
}

#[test]
fn neg_to_uint_error() {
    assert_nparse!(u8 => "integer is unsigned", -1);
}

#[test]
fn mismatched_tag_error() {
    assert_nparse!(u8 => "expected u8", 0u16);
    assert_nparse!(i8 => "expected i8", 0i16);
    assert_nparse!(f32 => "expected f32", 0.0f64);
}
