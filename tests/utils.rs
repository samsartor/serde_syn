use serde_derive::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize, Debug, PartialEq)]
pub enum MyEnum {
    MyVarient { data: Vec<String> }
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct SyntaxDemo {
    pub text: String,
    pub hashmap: HashMap<Vec<u32>, u32>,
    pub oneof: MyEnum,
}

#[macro_export]
macro_rules! assert_parse {
    (config = $c:expr, $t:ty, $x:expr, $($s:tt)*) => {
        match ::syn::parse::Parser::parse_str(
            ::serde_syn::parser::<$t>({
                #[allow(unused_imports)]
                use ::serde_syn::config::*;
                $c
            }),
            stringify!($($s)*),
        ) {
            Ok(x) => assert_eq!(x, $x),
            Err(e) => panic!("{:?}", e),
        }
    };
    (config = $c:expr, $t:ty, $x:expr) => {
        assert_parse!(config = $c, $t, $x, $x)
    };
    ($t:ty, $x:expr, $($s:tt)*) => {
        assert_parse!(config = RUSTY, $t, $x, $($s)*)
    };
    ($t:ty, $x:expr) => { assert_parse!(config = RUSTY, $t, $x) };
}

#[test]
#[should_panic]
fn assert_parse_can_fail() {
    assert_parse!(i32, 2 + 2);
}


#[macro_export]
macro_rules! assert_nparse {
    (config = $c:expr, $t:ty => $e:expr, $($s:tt)*) => {
        assert_eq!(
            format!("{}", ::syn::parse::Parser::parse_str(
                ::serde_syn::parser::<$t>({
                    #[allow(unused_imports)]
                    use ::serde_syn::config::*;
                    $c
                }),
                stringify!($($s)*),
            ).unwrap_err()),
            $e);
    };
    ($t:ty => $e:expr, $($s:tt)*) => { assert_nparse!(config = RUSTY, $t => $e, $($s)*) };
}

#[test]
#[should_panic]
fn assert_nparse_can_fail() {
    assert_nparse!(i32 => "", 4);
}
