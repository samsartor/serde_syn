#[macro_use]
mod utils;

use serde_derive::Deserialize;

#[test]
fn string_array() {
    assert_parse!(
        Vec<String>,
        vec!["Hello".to_owned(), "World".to_owned()],
        ["Hello", "World"]);
}

#[test]
fn empty_array() {
    assert_parse!(Vec<String>, Vec::<String>::new(), []);
}

#[test]
fn tuple() {
    assert_parse!((String, u32), ("Hello".to_owned(), 42), ("Hello", 42));
}

#[derive(Deserialize, Debug, PartialEq)]
struct Tuple(String, u32);

#[test]
fn tuple_struct() {
    assert_parse!(
        Tuple,
        Tuple("Hello".to_owned(), 42),
        Tuple("Hello", 42));
    assert_parse!(
        config = JSONY,
        Tuple,
        Tuple("Hello".to_owned(), 42),
        ("Hello", 42));
    assert_parse!(
        Tuple,
        Tuple("Hello".to_owned(), 42),
        Tuple("Hello", 42,));
}

#[test]
fn tuple_struct_errors() {
    assert_nparse!(Tuple => "expected identifier", ("Hello", 42));
}

#[derive(Deserialize, Debug, PartialEq)]
enum MyEnum {
    Tuple(String, u32),
}

#[test]
fn tuple_enum() {
    assert_parse!(
        config = RUSTY_STRICT,
        MyEnum,
        MyEnum::Tuple("Hello".to_owned(), 42),
        MyEnum::Tuple("Hello", 42));
    assert_parse!(
        config = JSONY,
        MyEnum,
        MyEnum::Tuple("Hello".to_owned(), 42),
        "Tuple" ("Hello", 42));
}

#[test]
fn trailing_comma() {
    assert_parse!(Vec<String>, vec!["Hello".to_owned()], ["Hello",]);
    assert_parse!((String,), ("Hello".to_owned(),), ("Hello",));
}
