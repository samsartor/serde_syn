#[macro_use]
mod utils;

use serde_bytes::ByteBuf;

#[test]
fn unicode_string() {
    assert_parse!(String, "Hello, World!");
    assert_parse!(String, "𝓗𝓮𝓵𝓵𝓸, 𝓦𝓸𝓻𝓵𝓭!");
    assert_parse!(String, "\n\r\t\\\u{211D}\x00");
    assert_parse!(String, r"\n\r\t\\\u{211D}\x00");
}

#[test]
fn byte_string() {
    assert_parse!(ByteBuf, b"\x00\x01\x02\x03");
    assert_parse!(ByteBuf, [0, 1u8, 2, 3]);
}


#[test]
fn array_type_errors() {
    assert_nparse!(ByteBuf => "expected u8", [0u32]);
    assert_nparse!(ByteBuf => "expected integer literal", ["Hello, World!"]);
    assert_nparse!(ByteBuf => "expected literal", [[0]]);
}

#[test]
fn empty_string() {
    assert_parse!(String, "");
    assert_parse!(ByteBuf, b"");
    assert_parse!(ByteBuf, []);
}
