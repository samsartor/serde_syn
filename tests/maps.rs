#[macro_use]
mod utils;

use utils::MyEnum;
use serde_derive::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize, Debug, PartialEq)]
struct MyStruct {
    a: String,
    b: Vec<u32>,
    c: (u32, (), String),
}

#[derive(Deserialize, Debug, Hash, PartialEq, Eq)]
#[serde(tag = "id", content = "data")]
enum MyTaggedEnum {
    Identifier(String),
}

fn counting_hashmap() -> HashMap<Vec<u32>, String> {
    let mut m = HashMap::new();
    m.insert(vec![1], "Single".to_owned());
    m.insert(vec![1, 2], "Double".to_owned());
    m.insert(vec![1, 2, 3], "Many".to_owned());
    m
}

fn text_hashmap() -> HashMap<String, String> {
    let mut m = HashMap::new();
    m.insert("hello".to_owned(), "World".to_owned());
    m.insert("foo".to_owned(), "Bar".to_owned());
    m
}

fn my_struct() -> MyStruct {
    MyStruct {
        a: "Hello".to_owned(),
        b: vec![1, 2, 3],
        c: (0, (), "World".to_owned()),
    }
}

fn my_enum() -> MyEnum {
    MyEnum::MyVarient {
        data: vec!["One".to_owned(), "Two".to_owned(), "Three".to_owned()],
    }
}

#[test]
fn structlike() {
    assert_parse!(
        config = RUSTY_STRICT,
        MyStruct,
        my_struct(),
        MyStruct {
            a: "Hello",
            b: [1, 2, 3],
            c: (0, (), "World"),
        });
    assert_parse!(
        config = RUSTY,
        HashMap<String, String>,
        text_hashmap(),
        {
            hello: "World",
            foo: "Bar",
        });
    assert_nparse!(
        config = RUSTY,
        HashMap<Vec<u32>, String> => "expected `=>` or `,`",
        {
            [1]: "Single",
            [1, 2]: "Double",
            [1, 2, 3]: "Many",
        });
}

#[test]
fn jsonlike() {
    assert_parse!(
        config = JSONY,
        MyStruct,
        my_struct(),
        {
            "a": "Hello",
            "b": [1, 2, 3],
            "c": (0, (), "World"),
        });
}

#[test]
fn matchlike() {
    assert_parse!(
        config = RUSTY,
        HashMap<Vec<u32>, String>,
        counting_hashmap(),
        {
            [1] => "Single",
            [1, 2] => "Double",
            [1, 2, 3] => "Many",
        });
    assert_parse!(
        config = RUSTY,
        HashMap<String, String>,
        text_hashmap(),
        {
            hello => "World",
            foo => "Bar",
        });
    assert_nparse!(
        config = RUSTY,
        MyStruct => "expected square brackets",
        MyStruct {
            a: "Hello",
            b => [1, 2, 3],
            c: (0, (), "World"),
        });
}

#[test]
fn tuple_list() {
    assert_parse!(
        config = RUSTY_STRICT,
        HashMap<Vec<u32>, String>,
        counting_hashmap(),
        [
            ([1], "Single"),
            ([1, 2], "Double"),
            ([1, 2, 3], "Many"),
        ]);
}

#[test]
fn attriblike() {
    assert_parse!(
        config = RUSTY_META,
        MyStruct,
        MyStruct {
            a: "Hello".to_owned(),
            b: vec![1, 2, 3],
            c: (0, (), "World".to_owned()),
        },
        a("Hello"),
        b(1, 2, 3),
        c(0, (), "World"));
    assert_nparse!(
        config = RUSTY_META,
        MyStruct => "expected one of: `=`, parentheses, `,`",
        a: "Hello",
        b: [1, 2, 3],
        c: (0, (), "World"));
}

fn str_ident_cfg(maps: bool, global: bool) -> serde_syn::config::Config {
    use serde_syn::config::{Config, KvOpts, GlobalOpts};
    let mut cfg: Config = Default::default();
    cfg.structs.set(KvOpts::STRING_IDENTS, maps);
    cfg.maps.set(KvOpts::STRING_IDENTS, maps);
    cfg.global.set(GlobalOpts::STRING_IDENTS, global);
    cfg
}

#[test]
fn identifiers() {
    assert_parse!(
        config = str_ident_cfg(false, false),
        MyTaggedEnum,
        MyTaggedEnum::Identifier("Hello".to_owned()),
        MyTaggedEnum {
            id: Identifier,
            data: "Hello",
        });
    assert_parse!(
        config = str_ident_cfg(false, true),
        MyTaggedEnum,
        MyTaggedEnum::Identifier("Hello".to_owned()),
        MyTaggedEnum {
            id: "Identifier",
            data: "Hello",
        });
    let mut map = HashMap::new();
    map.insert(MyTaggedEnum::Identifier("Hello".to_owned()), 7);
    assert_parse!(
        config = str_ident_cfg(false, true),
        HashMap<MyTaggedEnum, u32>,
        map,
        [
            (MyTaggedEnum {
                id: "Identifier",
                data: "Hello",
            }, 7),
        ]);
}

#[test]
fn ident_errors() {
    assert_nparse!(
        config = RUSTY,
        MyStruct => "expected identifier",
        MyStruct {
            "a": "Hello",
            b: [1, 2, 3],
            c: (0, (), "World"),
        });
    assert_nparse!(
        config = RUSTY,
        HashMap<String, String>  => "expected identifier",
        {
            "hello": "World",
            "foo": "Bar",
        });
    assert_nparse!(
        config = str_ident_cfg(false, false),
        MyTaggedEnum => "expected identifier",
        MyTaggedEnum {
            id: "Identifier",
            data: "Hello",
        });
    assert_nparse!(
        config = str_ident_cfg(true, false),
        HashMap<MyTaggedEnum, u32> => "expected identifier",
        {
            MyTaggedEnum {
                id: "Identifier",
                data: "Hello",
            } => 7,
        });
}

#[test]
fn named_enum() {
    assert_parse!(
        config = RUSTY_STRICT,
        MyEnum,
        my_enum(),
        MyEnum::MyVarient {
            data: ["One", "Two", "Three"],
        });
}

#[test]
fn unnamed_enum() {
    assert_parse!(
        config = JSONY,
        MyEnum,
        my_enum(),
        MyVarient {
            data: ["One", "Two", "Three"],
        });
    assert_parse!(
        config = JSONY,
        MyEnum,
        my_enum(),
        ::MyVarient {
            data: ["One", "Two", "Three"],
        });
    assert_parse!(
        config = JSONY,
        MyEnum,
        my_enum(),
        "MyVarient" {
            data: ["One", "Two", "Three"],
        });
    assert_parse!(
        config = JSONY,
        MyEnum,
        my_enum(),
        ::"MyVarient" {
            data: ["One", "Two", "Three"],
        });
}
