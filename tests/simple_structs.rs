#[macro_use]
mod utils;

use serde_derive::Deserialize;

#[test]
fn unit() {
    assert_parse!((), ());
}

#[test]
fn unit_nonempty_error() {
    assert_nparse!(() => "unexpected token", ("foo"));
}

#[test]
fn unit_nontuple_error() {
    assert_nparse!(() => "expected parentheses", 42);
}

#[test]
fn option_some() {
    let some = Some("Hello, World".to_owned());
    assert_parse!(
        Option<String>,
        some,
        Some("Hello, World"));
    assert_parse!(
        Option<String>,
        some,
        ::Some("Hello, World"));
    assert_parse!(
        Option<String>,
        some,
        Option::Some("Hello, World"));
    assert_parse!(
        Option<String>,
        some,
        "Hello, World");
}

#[test]
fn option_none() {
    assert_parse!(Option<String>, None);
    assert_parse!(Option<String>, None, ::None);
    assert_parse!(Option<String>, Option::None);
}

#[test]
fn prefixed_option() {
    assert_parse!(Option<String>,
        Option::Some("Hello, World".to_owned()),
        Option::Some("Hello, World"));
    assert_parse!(Option<String>, Option::None);
}

#[test]
fn nested_option() {
    assert_parse!(
        Option<Option<String>>,
        Some(Some("Hello, World".to_owned())),
        Some(Some("Hello, World")));
    assert_parse!(Option<Option<()>>, Some(Some(())));
    assert_parse!(Option<Option<()>>, Some(None));
    assert_parse!(Option<Option<()>>, Some(Some(())), Some(()));
    assert_parse!(Option<Option<()>>, Some(Some(())), ());
}

#[test]
fn option_name_error() {
    assert_nparse!(
        config = RUSTY_STRICT,
        Option<String> => "expected one of Option, Some, None",
        Som("Hello"));
    assert_nparse!(
        config = RUSTY,
        Option<String> => "expected literal",
        Som("Hello"));
}

#[test]
fn option_unit() {
    #[derive(Deserialize, Debug, PartialEq)]
    struct Foo { field: Option<()> };

    let some = Foo { field: Some(()) };
    let none = Foo { field: None };

    assert_parse!(
        config = RUSTY_META,
        Foo,
        some,
        field = ());
    assert_parse!(
        config = RUSTY_META,
        Foo,
        none,
        field = None);
    assert_parse!(
        config = RUSTY_META,
        Foo,
        some,
        field);
    assert_parse!(
        config = RUSTY_META,
        Foo,
        none,
        /* empty */);
}

#[test]
fn bare_some_error() {
    assert_nparse!(
        config = RUSTY_STRICT,
        Option<String> => "expected identifier",
        "Hello");
}

#[test]
fn none_extra_tokens_error() {
    assert_nparse!(Option<String> => "unexpected token", None());
}

#[test]
fn some_extra_tokens_error() {
    assert_nparse!(Option<String> => "unexpected token", Some("Hello", "World"));
    assert_nparse!(Option<String> => "unexpected token", Some("Hello".to_owned()));
    assert_nparse!(Option<u32> => "unexpected token", Some(1 + 2));
}

#[derive(Deserialize, Debug, PartialEq)]
struct Unit;

#[test]
fn unit_struct() {
    assert_parse!(Unit, Unit);
    assert_parse!(config = JSONY, Unit, Unit, ());
    assert_parse!(config = JSONY, Unit, Unit, Unit);
}

#[test]
fn unit_struct_errors() {
    assert_nparse!(Unit => "expected identifier", ());
    assert_nparse!(Unit => "unexpected token", Unit());
    assert_nparse!(config = JSONY, Unit => "unexpected token", ("Hello"));
    assert_nparse!(config = JSONY, Unit => "expected parentheses or identifier", 12);
}

#[derive(Deserialize, Debug, PartialEq)]
enum UnitEnum { Unit }

#[test]
fn unit_enum() {
    assert_parse!(UnitEnum, UnitEnum::Unit);
    assert_parse!(config = JSONY, UnitEnum, UnitEnum::Unit, ::Unit);
    assert_parse!(config = JSONY, UnitEnum, UnitEnum::Unit, Unit);
    assert_parse!(config = JSONY, UnitEnum, UnitEnum::Unit, "Unit");
}

#[derive(Deserialize, Debug, PartialEq)]
struct NewType(String);

#[test]
fn newtype_struct() {
    assert_parse!(NewType, NewType("Hello".to_owned()), NewType("Hello"));
    assert_parse!(
        config = JSONY,
        NewType,
        NewType("Hello".to_owned()),
        "Hello");
    assert_parse!(
        config = JSONY,
        NewType,
        NewType("Hello".to_owned()),
        NewType("Hello"));
}

#[test]
fn newtype_struct_errors() {
    assert_nparse!(NewType => "expected identifier", "Hello");
    assert_nparse!(NewType => "unexpected token", NewType("Hello", "World"));
    assert_nparse!(config = JSONY, NewType => "expected literal", ("Hello", "World"));
}


#[derive(Deserialize, Debug, PartialEq)]
enum NewTypeEnum { NewType(String) }

#[test]
fn newtype_enum() {
    let nt = NewTypeEnum::NewType("Hello".to_owned());
    assert_parse!(
        config = RUSTY_STRICT,
        NewTypeEnum,
        nt,
        NewTypeEnum::NewType("Hello"));
    assert_parse!(
        config = JSONY,
        NewTypeEnum,
        nt,
        ::NewType("Hello"));
    assert_parse!(
        config = JSONY,
        NewTypeEnum,
        nt,
        NewType("Hello"));
}
